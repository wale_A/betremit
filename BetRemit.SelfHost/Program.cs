﻿using BetRemit.API.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.SelfHost;

namespace BetRemit.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            //int denominationCount = 0;
            //denominationCount = Convert.ToInt32(Console.ReadLine());
            //int[] denominations = new int[denominationCount];
            //String[] bankNotes = Console.ReadLine().Split(' ');

            //for (int i = 0; i < denominationCount; i++)
            //{
            //    denominations[i] = Convert.ToInt32(bankNotes[i]);
            //}

            //int rent = Convert.ToInt32(Console.ReadLine());
            //long result = numberOfWaysToPay(denominations, denominationCount, rent);

            //Console.WriteLine(result);


            var config = new HttpSelfHostConfiguration("http://localhost:8088");
            WebAPIConfig.Configure(config);
            AutofacWebAPI.Initialize(config);
            RouteConfig.RegisterRoutes(config);
            EFconfig.Initialize();

            var host = new HttpSelfHostServer(config);
            host.OpenAsync().Wait();

            Console.WriteLine("BetRemit API hosted at {0}\r\n", config.BaseAddress);
            Console.ReadLine();

            host.CloseAsync().Wait();
        }

        //static long numberOfWaysToPay(int[] denominations, int denominationCount, int rent)
        //{
        //    int counter = 0;
        //    denominations = denominations.OrderBy(x => x).ToArray();
        //    for(int i = 0; i < denominationCount; i++)
        //    {
        //        if (denominations[i] % rent == 0)
        //            counter++;

        //        if (i == 0)
        //            continue;
        //        else
        //        {

        //        }
        //    }

        //    return counter;
        //}
    }
}
