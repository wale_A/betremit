﻿using BetRemit.Domain.Services;
using BetRemit.API.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Routing;

namespace BetRemit.API.MessageHandler
{
    /*for urls like
        GET api/punters/{punter-key}/cards
        GET api/punters/{punter-key}/cards/{card-key}
        
        ~ GET api/punters/{punter-key}/transactions
        GET api/punters/{punter-key}/cards/{card-key}/transactions ~
        
        i need this handler to ensure that the punter's key is correct before handling it over to the controller. save time doing it like this by preventing the request from reaching the HttpControllerDispatcher and i don't have to repeat code

        p.s. the request to those methods can also be put, patch
    */

    public class PunterCardsDispatcher : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // sinec we are here, we know the {key}(punter-key) route variable has been passed.
            IHttpRouteData routeData = request.GetRouteData();
            Guid punterKey = Guid.ParseExact(routeData.Values["key"].ToString(), "D");

            IPunterService punterService = request.GetPunterService();

            if (punterService.GetPunter(punterKey) == null)
                return Task.FromResult(request.CreateResponse(HttpStatusCode.NotFound));

            return base.SendAsync(request, cancellationToken);
        }
    }
}
