﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BetRemit.API.MessageHandler
{
    public class RequireHttpsMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if(request.RequestUri.Scheme != Uri.UriSchemeHttps) {
                HttpResponseMessage forbiddenResponseMessage = request.CreateResponse(HttpStatusCode.Forbidden);
                forbiddenResponseMessage.ReasonPhrase = "SSL Required";

                return Task.FromResult<HttpResponseMessage>(forbiddenResponseMessage);
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}
