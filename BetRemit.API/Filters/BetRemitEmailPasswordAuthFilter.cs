﻿using BetRemit.API.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace BetRemit.API.Filters
{
    public class BetRemitEmailPasswordAuthFilter : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            //look for credentials in request
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authenticationHeader = request.Headers.Authorization;

            if (authenticationHeader == null)
                return Task.FromResult(0);

            if (string.IsNullOrWhiteSpace(authenticationHeader.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return Task.FromResult(0);
            }

            Tuple<string, string> email_password = getEmailAndPassword(request);
            var membershipService = request.GetMembershipService();
            var validUserContext = membershipService.ValidateUserWithEmailPassword(email_password.Item1, email_password.Item2);
            IPrincipal principal = validUserContext.Principal;

            if (principal == null)
                context.ErrorResult = new AuthenticationFailureResult("Invalid Bearer key", request);
            else
                context.Principal = principal;

            return Task.FromResult(0);
        }

        private Tuple<string, string> getEmailAndPassword(HttpRequestMessage request)
        {
            var base64EncodedString = request.Headers.Authorization.Parameter;
            byte[] data = Convert.FromBase64String(base64EncodedString);
            var decodedString = Encoding.UTF8.GetString(data);

            var email_password = decodedString.Split(':');
            return Tuple.Create(email_password[0], email_password[1]);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var challenge = new AuthenticationHeaderValue("Bearer");
            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
            return Task.FromResult(0);
        }
    }
}
