﻿using BetRemit.Domain.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace BetRemit.API.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class PunterByUserAuthorizationFilter : AuthorizeAttribute
    {
        public PunterByUserAuthorizationFilter(string parameterName)
        {
            _parameterName = parameterName;
            //base.Roles
        }

        private string _parameterName;

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);

            //if there's a response in the actionCOntext's response property it means there's been a problem authenticating the user
            if (actionContext.Response == null)
            {
                HttpRequestMessage request = actionContext.Request;
                try
                {
                    Guid punterKey = getPunterKey(request.GetRouteData());
                    IPrincipal principal = Thread.CurrentPrincipal;
                    IPunterService punterService = request.GetPunterService();
                    bool punterIsAffiliatedToUser = principal.IsInRole("Admin") || punterService.IsPunterRelatedToUser(punterKey, principal.Identity.Name);

                    if (!punterIsAffiliatedToUser)
                        setErrorResponse(actionContext, request);
                }
                catch { setErrorResponse(actionContext, request); }
            }
        }

        private static void setErrorResponse(HttpActionContext actionContext, HttpRequestMessage request)
        {
            actionContext.Response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Specified Punter Not Found");
        }

        private Guid getPunterKey(IHttpRouteData httpRouteData)
        {
            var punterKey = httpRouteData.Values[_parameterName].ToString();
            return Guid.ParseExact(punterKey, "D");
        }
    }
}
