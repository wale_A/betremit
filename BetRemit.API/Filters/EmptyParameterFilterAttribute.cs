﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BetRemit.API.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class EmptyParameterFilterAttribute : ActionFilterAttribute
    {
        public string ParameterName { get; set; }
        public EmptyParameterFilterAttribute(string parameterName)
        {
            if (string.IsNullOrWhiteSpace(parameterName))
                throw new ArgumentNullException("Parameter Name is empty");

            ParameterName = parameterName;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            object parameterValue;

            if(actionContext.ActionArguments.TryGetValue(ParameterName, out parameterValue))
            {
                if(parameterValue == null)
                {
                    actionContext.ModelState.AddModelError(ParameterName, formatErrorMessage(ParameterName));

                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
                }
            }
        }

        private string formatErrorMessage(string parameterName)
        {
            return string.Format("The {0} cannot be null", parameterName);
        }
    }
}
