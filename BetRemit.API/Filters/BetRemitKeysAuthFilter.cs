﻿using BetRemit.API.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace BetRemit.API.Filters
{
    //authentication filter to check for live and test keys in the authentication header
    //use this authorization filter for API calls (USE in API Controllers/Action Methods)
    public class BetRemitKeysAuthFilter : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            //look for credentials in request
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authenticationHeader = request.Headers.Authorization;

            if (authenticationHeader == null) {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return Task.FromResult(0);
            }

            if (string.IsNullOrWhiteSpace(authenticationHeader.Parameter))  {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return Task.FromResult(0);
            }

            Tuple<string, string> key_referer = getBearerKey(request);
            var membershipService = request.GetMembershipService();
            var validUserContext = membershipService.ValidateUserWithKeyReferer(key_referer.Item1, key_referer.Item2);
            IPrincipal principal = validUserContext.Principal;

            if (principal == null)
                context.ErrorResult = new AuthenticationFailureResult("Invalid Bearer key", request);
            else
                context.Principal = principal;

            return Task.FromResult(0);
        }

        private Tuple<string, string> getBearerKey(HttpRequestMessage request)
        {
            var key = request.Headers.Authorization.Parameter;
            var referer = request.Headers.Referrer == null ? null : request.Headers.Referrer.ToString();

            return Tuple.Create(key, referer);
        }
        
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var challenge = new AuthenticationHeaderValue("Bearer");
            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
            return Task.FromResult(0);
        }
    }
}