﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace BetRemit.API.Filters
{
    public class AddChallengeOnUnauthorizedResult : IHttpActionResult
    {
        private AuthenticationHeaderValue Challenge;
        private IHttpActionResult InnerResult;

        public AddChallengeOnUnauthorizedResult(AuthenticationHeaderValue headerValue, IHttpActionResult httpActionResult)
        {
            Challenge = headerValue; InnerResult = httpActionResult;
        }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = await InnerResult.ExecuteAsync(cancellationToken);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                if (!response.Headers.WwwAuthenticate.Any(x => x.Scheme == Challenge.Scheme))
                    response.Headers.WwwAuthenticate.Add(Challenge);

            return response;
        }
    }
}
