﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace BetRemit.API.Filters
{
    internal class AuthenticationFailureResult : IHttpActionResult
    {
        private string ReasonPhrase;
        private HttpRequestMessage Request;

        public AuthenticationFailureResult(string reasonPhrase, HttpRequestMessage request)
        {
            this.ReasonPhrase = reasonPhrase;
            this.Request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(execute());
        }

        private HttpResponseMessage execute()
        {
            return new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized)
            {
                ReasonPhrase = ReasonPhrase,
                RequestMessage = Request
            };
        }
    }
}