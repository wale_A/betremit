﻿using BetRemit.API.Filters;
using BetRemit.API.MessageHandler;
using BetRemit.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Validation;
using System.Web.Http.Validation.Providers;

namespace BetRemit.API.Config
{
    public class WebAPIConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            //config.MessageHandlers.Add(new RequireHttpsMessageHandler());
            //config.MessageHandlers.Add(new BetRemitAuthHandler());

            //config.ParameterBindingRules.Insert(0, descriptor => typeof(IRequestCommand).IsAssignableFrom(descriptor.ParameterType) ? new FromUriAttribute().GetBinding(descriptor) : null);

            config.Filters.Add(new InvalidModelStateFilterAttribute());

            //var jqueryFormatter = config.Formatters.FirstOrDefault(x => x.GetType() == typeof(JQueryMvcFormUrlEncodedFormatter));
            //config.Formatters.Remove(config.Formatters.FormUrlEncodedFormatter);
            //config.Formatters.Remove(jqueryFormatter);
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            //foreach (var formatter in config.Formatters)
            //    formatter.RequiredMemberSelector = new SuppressedRequiredMemberSelector();

            //config.Services.Replace(typeof(IContentNegotiator), new DefaultContentNegotiator(excludeMatchOnTypeOnly: true));

            //config.Services.RemoveAll(typeof(ModelValidatorProvider), x => !(x is DataAnnotationsModelValidatorProvider));
        }
    }
}
