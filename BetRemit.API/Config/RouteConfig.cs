﻿using BetRemit.API.MessageHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Routing.Constraints;

namespace BetRemit.API.Config
{
    public class RouteConfig
    {
        public static void RegisterRoutes(HttpConfiguration config)
        {
            //var routes = config.Routes;
                
            //Pipelines
            HttpMessageHandler punterCardsPipeline = HttpClientFactory.CreatePipeline(new HttpControllerDispatcher(config), new[] { new PunterCardsDispatcher() });

            //Routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiRoute", 
                routeTemplate: "api/{controller}/{key}",
                defaults: new { key = RouteParameter.Optional },
                constraints: new { key = new GuidRouteConstraint() }
            );

            config.Routes.MapHttpRoute(
                name: "PunterCardsHttpRoute", 
                routeTemplate: "api/punter/{key}/cards/{cardKey}", 
                defaults: new {
                    controller = "PunterCards",
                    cardKey = RouteParameter.Optional
                }, 
                constraints: new {
                    key = new GuidRouteConstraint(),
                    cardKey = new GuidRouteConstraint()
                }, 
                handler: punterCardsPipeline );

            config.Routes.MapHttpRoute("Root", "", defaults: new { controller = "Users" });

            var cors = new System.Web.Http.Cors.EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
