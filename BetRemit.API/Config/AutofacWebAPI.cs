﻿using Autofac;
using Autofac.Integration.WebApi;
using BetRemit.Domain;
using BetRemit.Domain.EF;
using BetRemit.Domain.Services;
using System;
using System.Data.Entity;
using System.Reflection;
using System.Web.Http;

namespace BetRemit.API.Config
{
    public class AutofacWebAPI
    {
        public static Type ContainerService { get; private set; }

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            containerBuilder.RegisterType<EFContext>().As<DbContext>().InstancePerRequest();
            containerBuilder.RegisterGeneric(typeof(EFRepository<>)).As(typeof(IEntityRepository<>)).InstancePerRequest();
            containerBuilder.RegisterType<CryptoService>().As<ICryptoService>().InstancePerRequest();
            containerBuilder.RegisterType<MembershipService>().As<IMembershipService>().InstancePerRequest();
            containerBuilder.RegisterType<PunterService>().As<IPunterService>().InstancePerRequest();

            return containerBuilder.Build();
        }
    }
}
