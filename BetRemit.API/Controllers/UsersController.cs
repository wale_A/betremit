﻿using BetRemit.API.Extensions;
using BetRemit.API.Filters;
using BetRemit.API.Model.DTO;
using BetRemit.API.Model.RequestCommands;
using BetRemit.API.Model.RequestModel;
using BetRemit.Domain.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BetRemit.API.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IMembershipService _membershipService;

        public UsersController(IMembershipService membershipService)
        {
            _membershipService = membershipService;
        }

        //GET: api/users?page={page}&take={take}
        [Route("", Name = "GetAllUsers")]
        public PaginatedDTO<UserDTO> GetUsers([FromUri]PaginatedRequestCommand cmd)
        {
            var users = _membershipService.GetUsers(cmd.Page, cmd.Take);
            return users.ToPaginatedDTO(users.Select(x => x.ToUserDTO()));
        }

        //GET: api/users/{key}
        [Route("{key:guid}", Name = "GetSpecificUser")]
        public UserDTO GetUser(Guid key)
        {
            var user = _membershipService.GetUser(key);
            if (user == null)
                throw new HttpResponseException(System.Net.HttpStatusCode.NotFound);

            return user.ToUserDTO();
        }

        [Route("")]
        [EmptyParameterFilter("requestModel")]
        public HttpResponseMessage PostUser(UserRequestModel requestModel)
        {
            try
            {
                var createdUser = _membershipService.CreateUser(requestModel.Email, requestModel.Password, requestModel.Name, requestModel.CompanyName, requestModel.Phone, requestModel.Roles);
                if (!createdUser.IsSuccess)
                    return new HttpResponseMessage(HttpStatusCode.Conflict);

                var response = Request.CreateResponse(HttpStatusCode.Created, createdUser.Entity.ToUserDTO());
                var uriString = Url.Link("GetSpecificUser", new { key = createdUser.Entity.User.Key });
                response.Headers.Location = new Uri(uriString);
                return response;
            }
            catch(Exception ex)
            {
                //log exception
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Route("{key:guid}")]
        [EmptyParameterFilter("requestModel")]
        [HttpPut]
        public UserDTO PutUser(Guid key, UserUpdateRequestModel requestModel)
        {
            var user = _membershipService.GetUser(key);

            if (user == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var userWithRoles = _membershipService.UpdateUser(user.User, requestModel.Email, requestModel.CompanyName, requestModel.Name, requestModel.Phone);
            return userWithRoles.ToUserDTO();
        }

    }
}
