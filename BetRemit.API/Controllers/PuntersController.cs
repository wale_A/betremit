﻿using BetRemit.API.Extensions;
using BetRemit.API.Filters;
using BetRemit.API.Model.DTO;
using BetRemit.API.Model.RequestCommands;
using BetRemit.API.Model.RequestModel;
using BetRemit.Domain.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BetRemit.API.Controllers
{
    //[RoutePrefix("api/users/{userKey:guid}/punters")]
    [RoutePrefix("api/punters")]
    [BetRemitKeysAuthFilter]
    public class PuntersController : ApiController
    {
        private readonly IPunterService _punterService;
        private readonly IMembershipService _membershipService;

        public PuntersController(IPunterService punterService, IMembershipService membershipService)
        {
            _punterService = punterService;
            _membershipService = membershipService;
        }

        [Route("")]
        public PaginatedDTO<PunterDTO> GetPunters([FromUri]PaginatedRequestCommand cmd)
        {
            PaginatedDTO<PunterDTO> punterList = new PaginatedDTO<PunterDTO>();

            if (this.User.IsInRole("Admin")) {
                var punters = _punterService.GetPunters(cmd.Page, cmd.Take);
                punterList = punters.ToPaginatedDTO(punters.Select(x => x.ToPunterDTO()));
            }
            else {
                var userWithRole = _membershipService.GetUser(this.User.Identity.Name);
                var punters = _punterService.GetPunters(cmd.Page, cmd.Take, userWithRole.User.Key);
                punterList = punters.ToPaginatedDTO(punters.Select(x => x.ToPunterDTO()));
            }

            return punterList;
        }

        [Route("{punterKey:guid}", Name = "GetSinglePunter")]
        [PunterByUserAuthorizationFilter("punterKey")]
        public PunterDTO GetPunter(Guid punterKey)
        {
            var punter = _punterService.GetPunter(punterKey);
            if (punter == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return punter.ToPunterDTO();
        }

        [Route("")]
        [EmptyParameterFilter("requestModel")]
        public HttpResponseMessage PostPunter(PunterRequestModel requestModel)
        {
            var userWithRoles = _membershipService.GetUser(User.Identity.Name);
            var userKey = userWithRoles.User.Key;
            var createdPunterResult = _punterService.AddPunter(userKey, requestModel.ToPunter());

            if (!createdPunterResult.IsSuccess)
                return new HttpResponseMessage(HttpStatusCode.Conflict);

            var response = Request.CreateResponse(HttpStatusCode.Created, createdPunterResult.Entity.ToPunterDTO());
            response.Headers.Location = new Uri(Url.Link("GetSinglePunter", new { punterKey = createdPunterResult.Entity.Key }));

            return response;
        }

        [Route("{punterKey:guid}")]
        [EmptyParameterFilter("requestModel")]
        [PunterByUserAuthorizationFilter("punterKey")]
        public PunterDTO PutPunter(Guid punterKey, PunterUpdateRequestModel requestModel)
        {
            var punter = _punterService.GetPunter(punterKey);
            if (punter == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var updatedPunter = _punterService.UpdatePunter(requestModel.ToPunter(punter));
            return updatedPunter.ToPunterDTO();
        }

        [Route("{punterKey:guid}")]
        [PunterByUserAuthorizationFilter("punterKey")]
        public HttpResponseMessage DeletePunter(Guid punterKey)
        {
            var punter = _punterService.GetPunter(punterKey);
            if (punter == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var punterRemoveResult = _punterService.DeletePunter(punterKey);
            if (!punterRemoveResult.IsSuccess)
                return new HttpResponseMessage(HttpStatusCode.Conflict);

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
