﻿using BetRemit.API.Extensions;
using BetRemit.API.Filters;
using BetRemit.API.Model.DTO;
using BetRemit.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BetRemit.API.Controllers
{
    public class RolesController : ApiController
    {
        private readonly IMembershipService _membershipService;

        public RolesController(IMembershipService membershipService)
        {
            _membershipService = membershipService;
        }

        public IEnumerable<RoleDTO> GetRoles()
        {
            return _membershipService.GetRoles().Select(x => x.ToRoleDTO());
        }

        public RoleDTO GetRole(Guid key)
        {
            var role = _membershipService.GetRole(key);

            if (role == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return role.ToRoleDTO();
        }

        public RoleDTO GetRole(string name)
        {
            var role = _membershipService.GetRole(name);

            if (role == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return role.ToRoleDTO();
        }
    }
}
