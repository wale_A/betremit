﻿using BetRemit.API.Model.DTO;
using BetRemit.Domain.ServiceModels;
using System;
using System.Linq;

namespace BetRemit.API.Extensions
{
    internal static class UserWithRoleExtensions
    {
        internal static UserDTO ToUserDTO(this UserWithRoles userWithRoles)
        {
            if (userWithRoles == null)
            {
                throw new ArgumentNullException("User With Roles is NULL");
            }

            if (userWithRoles.User == null)
            {
                throw new ArgumentNullException("User in User With Roles object is NULL");
            }

            return new UserDTO { CompanyName = userWithRoles.User.CompanyName, CreatedOn = userWithRoles.User.CreatedOn, Email = userWithRoles.User.Email, IsLocked = userWithRoles.User.IsLocked, Key = userWithRoles.User.Key, IsLive = userWithRoles.User.IsLive, Name = userWithRoles.User.Name, LiveUrl = userWithRoles.User.LiveUrl, Phone = userWithRoles.User.Phone, TestPublicKey = userWithRoles.User.TestPublicKey, TestSecretKey = userWithRoles.User.TestSecretKey, Punters = userWithRoles.User.Punters.Select(x => x.ToPunterDTO()) };
        }
    }
}
