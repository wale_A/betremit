﻿using BetRemit.API.Model.DTO;
using BetRemit.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Extensions
{
    internal static class PaginatedListExtensions
    {
        internal static PaginatedDTO<T> ToPaginatedDTO<T, TEntity>(this PaginatedList<TEntity> source, IEnumerable<T> items)
            where T: IDto
        {
            return new PaginatedDTO<T> { items = items, TotalPageCount = source.TotalPageCount, HasPrevious = source.HasPreviousPage, HasNextPage = source.HasNextPage, PageIndex = source.PageIndex, PageSize = source.PageSize, TotalCount = source.TotalCount };
        }
    }
}
