﻿using BetRemit.API.Model.DTO;
using BetRemit.API.Model.RequestModel;
using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Extensions
{
    public static class PunterRequestModelExtensions
    {
        public static PunterDTO ToPunterDTO(this PunterRequestModel requestModel)
        {
            var punterDTO = new PunterDTO { Balance = requestModel.Balance, Email = requestModel.Email, Name = requestModel.Name, Phone = requestModel.Phone };

            return punterDTO;
        }

        public static Punter ToPunter(this PunterRequestModel requestModel)
        {
            return new Punter { Balance = requestModel.Balance, Email = requestModel.Email, Name = requestModel.Name, Phone = requestModel.Phone, BettingCompanyReference = requestModel.Reference };
        }

        public static Punter ToPunter(this PunterUpdateRequestModel requestModel, Punter existingPunter)
        {
            existingPunter.Phone = requestModel.Phone;
            existingPunter.Email = requestModel.Email;
            existingPunter.Balance = requestModel.Balance;
            return existingPunter;
        }
    }
}
