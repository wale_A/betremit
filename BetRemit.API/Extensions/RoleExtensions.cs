﻿using BetRemit.API.Model.DTO;
using BetRemit.Domain.Model;
using System;

namespace BetRemit.API.Extensions
{
    public static class RoleExtensions
    {
        public static RoleDTO ToRoleDTO(this Role role)
        {
            return new RoleDTO { Key = role.Key, Name = role.Name };
        }

    }
}
