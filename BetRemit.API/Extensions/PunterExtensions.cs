﻿using BetRemit.API.Model.DTO;
using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Extensions
{
    internal static class PunterExtensions
    {
        internal static PunterDTO ToPunterDTO(this Punter punter)
        {
            if (punter == null)
                throw new ArgumentNullException("Punter is NULL");

            return new PunterDTO { Balance = punter.Balance, CreatedOn = punter.CreatedOn, Email = punter.Email, Key = punter.Key, Name = punter.Name, Phone = punter.Phone, Reference = punter.BettingCompanyReference, CreatedByKey = punter.UserKey, CreatedBy = punter.User.ToUserDTO() };
        }
        
    }
}
