﻿using BetRemit.API.Model.DTO;
using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Extensions
{
    internal static class UserExtensions
    {
        internal static UserDTO ToUserDTO(this User user)
        {
            if (user == null)
                throw new ArgumentNullException("User is NULL");

            return new UserDTO { CompanyName = user.CompanyName, CreatedOn = user.CreatedOn, Email = user.Email, IsLocked = user.IsLocked, Key = user.Key, LiveUrl = user.LiveUrl, Name = user.Name, Phone = user.Phone };
        }
    }
}
