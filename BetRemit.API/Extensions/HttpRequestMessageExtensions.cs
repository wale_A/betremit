﻿using BetRemit.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace System.Net.Http
{ 
    public static class HttpRequestMessageExtensions
    {
        internal static IMembershipService GetMembershipService(this HttpRequestMessage request)
        {
            return request.GetService<IMembershipService>();
        }

        public static IPunterService GetPunterService(this HttpRequestMessage request)
        {
            return request.GetService<IPunterService>();
        }

        private static T GetService<T>(this HttpRequestMessage request)
        {
            IDependencyScope dependencyScope = request.GetDependencyScope();
            T service = (T)dependencyScope.GetService(typeof(T));
            return service;
        }
    }
}
