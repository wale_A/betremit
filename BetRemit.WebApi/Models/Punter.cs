﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Models
{
    public class Punter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CompanyReference { get; set; }
        public AccountState AccountState { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
