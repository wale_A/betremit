﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string CompanyName { get; set; }
        public string TestKey { get; set; }
        public string SecretKey { get; set; }
        public AccountState AccountState { get; set; }
        public TransactionState TransactionStatus { get; set; }

        public virtual ICollection<Punter> Punters { get; set; }
    }
}
