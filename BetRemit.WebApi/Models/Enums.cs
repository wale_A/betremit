﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Models
{
    public enum AccountState { Enabled, Disabled }
    public enum TransactionState { Live, Test }
}
