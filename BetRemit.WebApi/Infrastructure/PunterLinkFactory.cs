﻿using BetRemit.WebApi.Controllers;
using BetRemit.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public class PunterLinkFactory : LinkFactory<PunterController>
    {
        private const string Prefix = "http://betremit.com/documentation#";

        public new class Rels: LinkFactory.Rels
        {
            public const string Disable = Prefix + Actions.Disable;
            public const string Enable = Prefix + Actions.Enable;
        }

        public class Actions
        {
            public const string Disable = "disable";
            public const string Enable = "enable";
        }

        public PunterLinkFactory(HttpRequestMessage request) : base(request)
        { }

        public Link Enable(string reference)
        {
            return GetLink<PunterController>(Rels.Enable, reference, Actions.Enable);
        }

        public Link Disable(string reference)
        {
            return GetLink<PunterController>(Rels.Disable, reference, Actions.Disable);
        }
    }
}
