﻿using BetRemit.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;

namespace BetRemit.WebApi.Infrastructure
{
    public abstract class LinkFactory
    {
        private readonly UrlHelper _urlHelper;
        private readonly string _controllerName;
        private const string DefaultApi = "DefaultApi";

        protected LinkFactory(HttpRequestMessage request, Type controllerType)
        {
            _urlHelper = new UrlHelper(request);
            _controllerName = getControllerName(controllerType);
        }

        private string getControllerName(Type controllerType)
        {
            var name = controllerType.Name;
            return name.Substring(0, name.Length - "controller".Length).ToLower();
        }

        protected Link GetLink<TController>(string rel, object id, string action, string route = DefaultApi)
        {
            var url = GetUri(new { controller = getControllerName(typeof(TController)), id, action }, route);
            return new Link { Action = action, Href = url, Rel = rel };
        }

        private Uri GetUri(object routeValues, string route)
        {
            return new Uri(_urlHelper.Link(route, routeValues));
        }

        public Link Self(string id, string route = DefaultApi)
        {
            return new Link { Rel = Rels.Self, Href = GetUri(new { controller = _controllerName, id }, route) };
        }

        public  class Rels
        {
            public const string Self = "self";
        }
    }

    public abstract class LinkFactory<TController>: LinkFactory
    {
        public LinkFactory(HttpRequestMessage request) : 
            base(request, typeof(TController)) { }
    }

}
