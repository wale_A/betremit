﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public interface IDataContext
    {
        Task<IEnumerable<TModel>> FindAsync<TModel>();
        Task<T> FindAsync<T>(int id);
        Task<IEnumerable<T>> FindAsyncQuery<T>(string text);
        Task UpdateAsync<T>(T model);
        Task DeleteAsync<T>(int id);
        Task CreteAsync<TModel>(TModel model);
    }
}
