﻿using BetRemit.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public class InMemoryDataContext : IDataContext
    {
        private IList<User> _users;
        private int _id = 0;
        private static Type _userType = typeof(User);

        public InMemoryDataContext()
        {
            _users = new List<User>();
            _users.Add(new User { Id = 1, AccountState = AccountState.Enabled, CompanyName = "bet9ja", Email = "isiaq.oa@gmail.com", PasswordHash = "hfad", TransactionStatus = TransactionState.Live, SecretKey = "drge45tgervtr", TestKey = "2345674hegr" });
        }

        public Task CreteAsync<User>(User model)
        {
            model.Id
        }

        public Task DeleteAsync<T>(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> FindAsync<T>()
        {
            throw new NotImplementedException();
        }

        public Task<T> FindAsync<T>(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> FindAsyncQuery<T>(string text)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync<T>(T model)
        {
            throw new NotImplementedException();
        }
    }
}
