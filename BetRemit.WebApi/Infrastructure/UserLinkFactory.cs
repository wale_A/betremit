﻿using BetRemit.WebApi.Controllers;
using BetRemit.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public class UserLinkFactory : LinkFactory<UserController>
    {
        private const string Prefix = "http://betremit.com/documentation#";

        public new class Rels: LinkFactory.Rels
        {
            public const string Disable = Prefix + Actions.Disable;
            public const string Enable = Prefix + Actions.Enable;
            public const string Live = Prefix + Actions.Live;
            public const string Test = Prefix + Actions.Test;
            public const string User = Prefix + "user";
            public const string Users = Prefix + "users";

        }

        public class Actions
        {
            public const string Disable = "disable";
            public const string Enable = "enable";
            public const string Live = "go-live";
            public const string Test = "go-test";
        }

        public UserLinkFactory(HttpRequestMessage request) : base(request)
        { }

        public Link Enable(string id)
        {
            return GetLink<UserController>(Rels.Enable, id, Actions.Enable);
        }

        public Link Disable(string id)
        {
            return GetLink<UserController>(Rels.Disable, id, Actions.Disable);
        }

        public Link Live(string id)
        {
            return GetLink<UserController>(Rels.Live, id, Actions.Live);
        }

        public Link Test(string id)
        {
            return GetLink<UserController>(Rels.Test, id, Actions.Test);
        }
    }
}
