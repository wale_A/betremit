﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public interface IStateFactory<TModel, TState>
    {
        TState Create(TModel model);
    }
}
