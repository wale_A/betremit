﻿using BetRemit.WebApi.Models;
using BetRemit.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.Infrastructure
{
    public class PunterStateFactory : IStateFactory<Punter, PunterState>
    {
        private readonly PunterLinkFactory _linkFactory;

        public PunterStateFactory(PunterLinkFactory linkFactory)
        {
            _linkFactory = linkFactory;
        }

        public PunterState Create(Punter punter)
        {
            var model = new PunterState
            {
                Id = punter.Id,
                CompanyReference = punter.CompanyReference,
                Email = punter.Email,
                Name = punter.Name,
                Phone = punter.Phone,
                CompanyName = punter.User.CompanyName
            };

            model.Links.Add(_linkFactory.Self(punter.Id.ToString()));

            switch (punter.AccountState)
            {
                case AccountState.Disabled:
                    model.Links.Add(_linkFactory.Enable(punter.CompanyReference));
                    break;
                case AccountState.Enabled:
                    model.Links.Add(_linkFactory.Disable(punter.CompanyReference));
                    break;
            }
            return model;
        }
    }
}
