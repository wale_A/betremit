﻿using BetRemit.WebApi.Models;
using BetRemit.WebApi.ViewModels;
using System.Linq;

namespace BetRemit.WebApi.Infrastructure
{
    public class UserStateFactory : IStateFactory<User, ViewModels.UserState>
    {
        private readonly UserLinkFactory _linkFactory;
        PunterStateFactory _punterStateFactory;

        public UserStateFactory(UserLinkFactory linkFactory, PunterStateFactory punterStateFactory)
        {
            _linkFactory = linkFactory;
            _punterStateFactory = punterStateFactory;
        }

        public UserState Create(User user)
        {
            var model = new ViewModels.UserState
            {
                Id = user.Id,
                CompanyName = user.CompanyName,
                Email = user.Email
            };
            model.Punters = user.Punters.Select(x => _punterStateFactory.Create(x)).ToList();

            model.Links.Add(_linkFactory.Self(user.Id.ToString()));

            switch (user.AccountState)
            {
                case AccountState.Disabled:
                    model.Links.Add(_linkFactory.Enable(user.Id.ToString()));
                    break;
                case AccountState.Enabled:
                    model.Links.Add(_linkFactory.Disable(user.Id.ToString()));
                    break;
            }

            switch (user.TransactionStatus)
            {
                case TransactionState.Live:
                    model.Links.Add(_linkFactory.Test(user.Id.ToString()));
                    break;
                case TransactionState.Test:
                    model.Links.Add(_linkFactory.Live(user.Id.ToString()));
                    break;
            }

            return model;
        }

    }
}
