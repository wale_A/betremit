﻿using Autofac;
using Autofac.Integration.WebApi;
using BetRemit.WebApi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BetRemit.WebApi
{
    public static class WebApiConfiguration
    {
        public static void Configure(HttpConfiguration config, IDataContext dbContext = null)
        {
            config.Routes.MapHttpRoute("Root", "", new { controller = "Home" });
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
            ConfigureFormatters(config);
            ConfigureAutoAfac(config, dbContext);
            EnableCors(config);
        }

        private static void EnableCors(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }

        private static void ConfigureAutoAfac(HttpConfiguration config, IDataContext dbContext)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof().Assembly);

            if (dbContext == null)
                builder.RegisterType<>().As<IDataContext>().SingleInstance();
            else
                builder.RegisterInstance(dbContext);


        }

        private static void ConfigureFormatters(HttpConfiguration config)
        {
            throw new NotImplementedException();
        }
    }
}
