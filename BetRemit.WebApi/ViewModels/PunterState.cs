﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.ViewModels
{
    public class PunterState
    {
        public PunterState()
        {
            Links = new List<Link>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CompanyReference { get; set; }
        public string CompanyName { get; set; }

        public ICollection<Link> Links { get; set; }
    }
}
