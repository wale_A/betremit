﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.WebApi.ViewModels
{
    public class UserState
    {
        public UserState()
        {
            Punters = new List<PunterState>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }

        public ICollection<PunterState> Punters { get; set; }
        public ICollection<Link> Links { get; set; }
    }
}
