﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiContrib.CollectionJson;

namespace BetRemit.WebApi.ViewModels
{
    public class UsersState : IReadDocument
    {
        public UsersState()
        {
            Links = new List<Link>();
        }

        public IEnumerable<UserState> Users { get; set; }
        public IList<Link> Links { get; set; }

        Collection IReadDocument.Collection
        {
            get
            {
                var collection = new Collection();
                collection.Href = Links.SingleOrDefault(x => x.Rel == )
                return null;
            }
        }
    }
}
