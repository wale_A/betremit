﻿using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.ServiceModels
{
    public class UserWithRoles
    {
        public IEnumerable<Role> Roles { get; set; }
        public User User { get; set; }
    }
}
