﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Services
{
    public static class IPunterServiceExtensions
    {
        public static bool IsPunterRelatedToUser(this IPunterService punterService, Guid punterKey, string userEmail)
        {
            var punter = punterService.GetPunter(punterKey);
            return punter.User.Email.Equals(userEmail, StringComparison.InvariantCultureIgnoreCase); 
        }
    }
}
