﻿using BetRemit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Model
{
    public class Punter : IEntity
    {
        [Key]
        public Guid Key { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string Phone { get; set; }
        public string BettingCompanyReference { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastUpdatedOn { get; set; }

        public Guid UserKey { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<VirtualCard> VirtualCards { get; set; }

    }
}
