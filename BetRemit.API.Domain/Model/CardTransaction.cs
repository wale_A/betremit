﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Model
{
    public class CardTransaction : IEntity
    {
        [Key]
        public Guid Key { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedOn { get; set; }

        public Guid CardKey { get; set; }
        public virtual VirtualCard Card { get; set; }
    }
}
