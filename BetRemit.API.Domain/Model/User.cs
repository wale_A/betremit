﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Model
{
    public class User : IEntity
    {
        [Key]
        public Guid Key { get; set; }

        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string CompanyName { get; set; }
        public string HashedPassword { get; set; }
        public string Salt {get; set; }
        public bool IsLocked { get; set; }
        public bool IsLive { get; set; }
        public string TestPublicKey { get; set; }
        public string TestSecretKey { get; set; }
        public string LivePublicKey { get; set; }
        public string LiveSecretKey { get; set; }
        public string PasswordChangeCode { get; set; }
        public string LiveUrl { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? LastUpdatedOn { get; set; }

        public virtual ICollection<Punter> Punters { get; set; }
        public virtual ICollection<UserInRole> UserInRoles { get; set; }

        public User()
        {
            Punters = new HashSet<Punter>();
            UserInRoles = new HashSet<UserInRole>();
        }
    }
}
