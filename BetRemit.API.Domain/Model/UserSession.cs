﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Model
{
    public class UserSession : IEntity
    {
        [Key]
        public Guid Key { get; set; }

        public Guid UserKey { get; set; }
        public Guid SessionKey { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? UpdateOn { get; set; }
        public DateTime SessionEnd { get; set; }
        public bool SessionEnded { get; set; }

        public User User { get; set; }
    }
}
