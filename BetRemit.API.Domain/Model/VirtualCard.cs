﻿using BetRemit.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Model
{
    public class VirtualCard : IEntity
    {
        [Key]
        public Guid Key { get; set; }
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public string CVV { get; set; }
        public bool IsLocked { get; set; }

        public Guid PunterKey { get; set; }
        public virtual Punter Punter { get; set; }

        public virtual ICollection<CardTransaction>CardTransactions { get; set; }
    }
}
