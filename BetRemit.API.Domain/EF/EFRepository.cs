﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace BetRemit.Domain.EF
{
	public class EFRepository<T> : IEntityRepository<T>
		where T: class, IEntity, new()
    {
        readonly DbContext _db;

        public EFRepository(DbContext context)
        {
            _db = context ?? throw new ArgumentNullException("EF Entity Context");
        }

        public IQueryable<T> All => GetAll();

        public void Add(T entity)
        {
            DbEntityEntry entry = _db.Entry<T>(entity);
            _db.Set<T>().Add(entity);
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _db.Set<T>();
			foreach(var property in includeProperties)
                query = query.Include(property);

            return query;
        }

        public void Delete(T entity)
        {
            DbEntityEntry entry = _db.Entry<T>(entity);
            entry.State = EntityState.Deleted;
        }

        public void Edit(T entity)
        {
            DbEntityEntry entry = _db.Entry<T>(entity);
            entry.State = EntityState.Modified;
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _db.Set<T>().Where(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return _db.Set<T>();
        }

		public T GetSingle(Guid key)
        {
            return GetAll().FirstOrDefault(x => x.Key == key);
        }

        public PaginatedList<T> Paginate<TKey>(int pageIndex, int pageSize, Expression<Func<T, TKey>> keySelector)
        {
            return Paginate(pageIndex, pageSize, keySelector, null);
        }

        public PaginatedList<T> Paginate<TKey>(int pageIndex, int pageSize, Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = AllIncluding(includeProperties).OrderBy(keySelector);
            query = (predicate == null) ? query : query.Where(predicate);
            return query.ToPaginatedList(pageIndex, pageSize);
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}