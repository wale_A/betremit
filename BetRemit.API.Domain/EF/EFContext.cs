﻿using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.EF
{
    public class EFContext : DbContext//, IDb
    {
        public EFContext() : base("BetRemit") { }

        public IDbSet<User> Users { get; set; }
        public IDbSet<Role> Roles { get; set; }
        public IDbSet<UserInRole> UserInRoles { get; set; }
        public IDbSet<VirtualCard> Cards { get; set; }
        public IDbSet<CardTransaction> Transactions { get; set; }
    }
}