﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain
{
    public interface IEntity
    {
        Guid Key { get; set; }
    }
}
