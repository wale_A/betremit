﻿using BetRemit.Domain;
using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.RepositoryExtensions
{
    public static class PunterRepositoryExtensions
    {
        public static IQueryable<Punter> GetPuntersByUserKey(this IEntityRepository<Punter> entityRepository, Guid key)
        {
            return entityRepository.AllIncluding(x => x.User, x => x.VirtualCards).Where(x => x.UserKey == key);
        }
    }
}
