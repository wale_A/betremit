﻿using BetRemit.Domain.Model;
using System.Linq;

namespace BetRemit.Domain.RepositoryExtensions
{
    public static class RoleRepositoryExtensions
    {
		public static Role GetRoleByRoleName(this IEntityRepository<Role> roleRepository, string roleName)
        {
            var role = roleRepository.FindBy(x => x.Name == roleName).FirstOrDefault();
            return role;
        }
    }

}