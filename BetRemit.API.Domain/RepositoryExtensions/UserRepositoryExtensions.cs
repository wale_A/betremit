﻿using BetRemit.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.RepositoryExtensions
{
    public static class UserRepositoryExtensions
    {
        public static User GetUserByEmail(this IEntityRepository<User> userRepository, string email)
        {
            return userRepository.FindBy(x => x.Email == email).FirstOrDefault();
        }

        public static User GetUserByKey(this IEntityRepository<User> userRepository, string key)
        {
            return userRepository.FindBy(x => x.LiveSecretKey == key || x.TestSecretKey == key).FirstOrDefault();
        }
    }
}
