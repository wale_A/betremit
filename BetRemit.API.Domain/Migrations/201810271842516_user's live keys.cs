namespace BetRemit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userslivekeys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "TestPublicKey", c => c.String());
            AddColumn("dbo.Users", "TestSecretKey", c => c.String());
            AddColumn("dbo.Users", "LivePublicKey", c => c.String());
            AddColumn("dbo.Users", "LiveSecretKey", c => c.String());
            DropColumn("dbo.Users", "TestKey");
            DropColumn("dbo.Users", "LiveKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "LiveKey", c => c.String());
            AddColumn("dbo.Users", "TestKey", c => c.String());
            DropColumn("dbo.Users", "LiveSecretKey");
            DropColumn("dbo.Users", "LivePublicKey");
            DropColumn("dbo.Users", "TestSecretKey");
            DropColumn("dbo.Users", "TestPublicKey");
        }
    }
}
