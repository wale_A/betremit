namespace BetRemit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bettingcompanyreferenceinpuntertable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Punters", "BettingCompanyReference", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Punters", "BettingCompanyReference");
        }
    }
}
