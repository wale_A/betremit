namespace BetRemit.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Domain.Model;

    public sealed class Configuration : DbMigrationsConfiguration<Domain.EF.EFContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Domain.EF.EFContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Roles.AddOrUpdate(x => x.Name,
                new Role { Name= "Admin", Key = Guid.NewGuid() }, new Role { Name = "BettingCompany", Key = Guid.NewGuid() }, new Role { Name = "User", Key = Guid.NewGuid() });
        }
    }
}
