// <auto-generated />
namespace BetRemit.Domain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class userslivekeys : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(userslivekeys));
        
        string IMigrationMetadata.Id
        {
            get { return "201810271842516_user's live keys"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
