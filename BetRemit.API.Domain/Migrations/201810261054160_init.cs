namespace BetRemit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VirtualCards",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        Name = c.String(),
                        CardNumber = c.String(),
                        CVV = c.String(),
                        IsLocked = c.Boolean(nullable: false),
                        PunterKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.Punters", t => t.PunterKey, cascadeDelete: true)
                .Index(t => t.PunterKey);
            
            CreateTable(
                "dbo.CardTransactions",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedOn = c.DateTime(nullable: false),
                        CardKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.VirtualCards", t => t.CardKey, cascadeDelete: true)
                .Index(t => t.CardKey);
            
            CreateTable(
                "dbo.Punters",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Phone = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        LastUpdatedOn = c.DateTime(),
                        UserKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.Users", t => t.UserKey, cascadeDelete: true)
                .Index(t => t.UserKey);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        Email = c.String(),
                        Name = c.String(),
                        CompanyName = c.String(),
                        Phone = c.String(),
                        HashedPassword = c.String(),
                        Salt = c.String(),
                        IsLocked = c.Boolean(nullable: false),
                        IsLive = c.Boolean(nullable: false),
                        TestKey = c.String(),
                        LiveKey = c.String(),
                        PasswordChangeCode = c.String(),
                        LiveUrl = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        LastUpdatedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Key);
            
            CreateTable(
                "dbo.UserInRoles",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        UserKey = c.Guid(nullable: false),
                        RoleKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.Roles", t => t.RoleKey, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserKey, cascadeDelete: true)
                .Index(t => t.UserKey)
                .Index(t => t.RoleKey);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Key = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Key);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VirtualCards", "PunterKey", "dbo.Punters");
            DropForeignKey("dbo.UserInRoles", "UserKey", "dbo.Users");
            DropForeignKey("dbo.UserInRoles", "RoleKey", "dbo.Roles");
            DropForeignKey("dbo.Punters", "UserKey", "dbo.Users");
            DropForeignKey("dbo.CardTransactions", "CardKey", "dbo.VirtualCards");
            DropIndex("dbo.UserInRoles", new[] { "RoleKey" });
            DropIndex("dbo.UserInRoles", new[] { "UserKey" });
            DropIndex("dbo.Punters", new[] { "UserKey" });
            DropIndex("dbo.CardTransactions", new[] { "CardKey" });
            DropIndex("dbo.VirtualCards", new[] { "PunterKey" });
            DropTable("dbo.Roles");
            DropTable("dbo.UserInRoles");
            DropTable("dbo.Users");
            DropTable("dbo.Punters");
            DropTable("dbo.CardTransactions");
            DropTable("dbo.VirtualCards");
        }
    }
}
