﻿using BetRemit.Domain;
using BetRemit.Domain.Model;
using BetRemit.Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Services
{
    public interface IPunterService
    {
        PaginatedList<Punter> GetPunters(int page, int take);
        PaginatedList<Punter> GetPunters(int page, int take, Guid userKey);
        Punter GetPunter(Guid key);
        OperationResult<Punter> AddPunter(Guid userKey, Punter punter);
        Punter UpdatePunter(Punter punter);
        OperationResult DeletePunter(Guid key);


    }
}
