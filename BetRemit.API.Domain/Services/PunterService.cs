﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetRemit.Domain;
using BetRemit.Domain.Model;
using BetRemit.Domain.RepositoryExtensions;
using BetRemit.Domain.ServiceModels;
using BetRemit.Domain.Services;

namespace BetRemit.Domain.Services
{
    public class PunterService : IPunterService
    {
        private readonly IEntityRepository<Punter> _punterRepository;
        private readonly IEntityRepository<User> _userRepository;
        private readonly IMembershipService _membershipService;

        public PunterService(IEntityRepository<Punter> punterRepository, IEntityRepository<User> userRepository, IMembershipService membershipService)
        {
            _userRepository = userRepository;
            _punterRepository = punterRepository;
            _membershipService = membershipService;
        }

        public OperationResult<Punter> AddPunter(Guid userKey, Punter punter)
        {
            var userResult = _membershipService.GetUser(userKey);
            if (userResult == null 
                //|| userResult.Roles.Any(x => x.Name.Equals("bettingCompany", StringComparison.OrdinalIgnoreCase)) 
                || _punterRepository.FindBy(x => x.Email == punter.Email).FirstOrDefault() != null)
                return new OperationResult<Punter>(false);

            punter.UserKey = userKey;
            punter.CreatedOn = DateTime.Now;
            punter.Key = Guid.NewGuid();

            _punterRepository.Add(punter);
            _punterRepository.Save();

            //create virtual card for punter

            punter.User = userResult.User;
            return new OperationResult<Punter>(true) { Entity = punter };
        }

        public OperationResult DeletePunter(Guid key)
        {
            var punter = _punterRepository.GetSingle(key);
            if (punter == null)
                return new OperationResult(false);

            _punterRepository.Delete(punter);
            _punterRepository.Save();

            //disable punter's virtual card

            return new OperationResult(true);
        }

        public Punter GetPunter(Guid key)
        {
            return getPunterWithIncludedProperties(key);
            //return _punterRepository.GetSingle(key);
        }

        public PaginatedList<Punter> GetPunters(int page, int take)
        {
            var punters = getPuntersWithIncludedProperties().ToPaginatedList(page, take);
            //var punters = _punterRepository.Paginate(page, take, x => x.CreatedOn);
            return punters;
        }

        public PaginatedList<Punter> GetPunters(int page, int take, Guid userKey)
        {
            var punters = _punterRepository.GetPuntersByUserKey(userKey).OrderBy(x => x.CreatedOn).ToPaginatedList(page, take);

            return punters;
        }

        public Punter UpdatePunter(Punter punter)
        {
            punter.LastUpdatedOn = DateTime.Now;
            _punterRepository.Edit(punter);
            _punterRepository.Save();

            return getPunterWithIncludedProperties(punter.Key);
        }

        private IQueryable<Punter> getPuntersWithIncludedProperties()
        {
            return _punterRepository.AllIncluding(x => x.User, x => x.VirtualCards).OrderBy(x => x.CreatedOn);
        }

        private Punter getPunterWithIncludedProperties(Guid key)
        {
            var punter = getPuntersWithIncludedProperties().FirstOrDefault(x => x.Key == key);
            return punter;
        }
    }
}
