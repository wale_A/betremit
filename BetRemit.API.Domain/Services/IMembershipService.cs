﻿using BetRemit.Domain.Model;
using BetRemit.Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Services
{
    public interface IMembershipService
    {
        ValidUserContext ValidateUserWithEmailPassword(string email, string password);

        ValidUserContext ValidateUserWithKeyReferer(string key, string referer);

        OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone);

        OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone, string role);

        OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone, string[] roles);

        UserWithRoles UpdateUser(User user, string email, string companyName, string name, string phone);

        bool ChangePassword(string email, string authCode, string newPassword);

        bool AddToRole(string email, string roleName);

        bool AddToRole(Guid key, string roleName);

        bool RemoveFromRole(Guid key, string roleName);

        bool RemoveFromRole(string email, string roleName);

        IEnumerable<Role> GetRoles();

        Role GetRole(string name);

        Role GetRole(Guid key);

        PaginatedList<UserWithRoles> GetUsers(int pageIndex, int pageSize);

        UserWithRoles GetUser(Guid key);

        UserWithRoles GetUser(string email);
    }
}
