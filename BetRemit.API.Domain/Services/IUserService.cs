﻿using BetRemit.Domain.Model;
using BetRemit.Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.Domain.Services
{
    public interface IUserService
    {
        PaginatedList<User> GetUser(int page, int take);
        User GetUser(Guid key);
    }
}
