﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using BetRemit.Domain.Model;
using BetRemit.Domain.RepositoryExtensions;
using BetRemit.Domain.ServiceModels;

namespace BetRemit.Domain.Services
{
    public class MembershipService : IMembershipService
    {
        private readonly IEntityRepository<Role> _roleRepository;
        private readonly IEntityRepository<User> _userRepository;
        private readonly IEntityRepository<UserInRole> _userInRoleRepository;
        private readonly ICryptoService _cryptoService;

        public MembershipService(IEntityRepository<User> userRepository, IEntityRepository<Role> roleRepository, IEntityRepository<UserInRole> userInRoleRepository, ICryptoService cryptoService)
        {
            _roleRepository = roleRepository;
            _userInRoleRepository = userInRoleRepository;
            _userRepository = userRepository;
            _cryptoService = cryptoService;
        }
        
        public bool AddToRole(string email, string roleName)
        {
            var user = _userRepository.GetUserByEmail(email);
            if(user != null)
            {
                addUserToRole(user, roleName);
                return true;
            }
            return false;
        }

        public bool AddToRole(Guid key, string roleName)
        {
            var user = _userRepository.GetSingle(key);
            if(user != null)
            {
                addUserToRole(user, roleName);
                return true;
            }
            return false;
        }

        public bool ChangePassword(string email, string authCode, string newPassword)
        {
            var user = _userRepository.GetUserByEmail(email);
            if(user != null)
            {
                if(user.PasswordChangeCode == authCode)
                {
                    var hashedPassword_salt = generateUserPassword_HashedPassword_Salt(newPassword);
                    user.HashedPassword = hashedPassword_salt.Item1;
                    user.Salt = hashedPassword_salt.Item2;
                    UpdateUser(user, user.Email, user.CompanyName, user.Name, user.Phone);
                    return true;
                }
            }
            return false;
        }

        public OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone)
        {
            return CreateUser(email, password, name, companyName, phone, new string[] { "User" });
        }

        public OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone, string role)
        {
            return CreateUser(email, password, name, companyName, phone, new string[] { role });
        }

        public OperationResult<UserWithRoles> CreateUser(string email, string password, string name, string companyName, string phone, string[] roles)
        {
            var exisitingUser = _userRepository.FindBy(x => x.Email == email).FirstOrDefault();
            if (exisitingUser != null)
                return new OperationResult<UserWithRoles>(false);

            var hashedPassword_salt = generateUserPassword_HashedPassword_Salt(password);

            //create test key
            var user = new User
            {
                Email = email, HashedPassword = hashedPassword_salt.Item1, Salt = hashedPassword_salt.Item2, CreatedOn = DateTime.Now, IsLocked = false, CompanyName = companyName, Name = name, Phone = phone, Key = Guid.NewGuid(), LivePublicKey = string.Format("lv_pk_{0}", Guid.NewGuid().ToString("N")), LiveSecretKey = string.Format("lv_sk_{0}", Guid.NewGuid().ToString("N")), TestPublicKey = string.Format("ts_pk_{0}", Guid.NewGuid().ToString("N")), TestSecretKey = string.Format("ts_sk_{0}", Guid.NewGuid().ToString("N"))
            };

            _userRepository.Add(user);
            _userRepository.Save();

            if (roles != null && roles.Length > 0)
                foreach (var role in roles)
                    addUserToRole(user, role);
            else
                addUserToRole(user, "User");

            return new OperationResult<UserWithRoles>(true) { Entity = getUserWithRoles(user) };
        }

        private void addUserToRole(User user, string roleName)
        {
            var role = _roleRepository.GetRoleByRoleName(roleName);

            if(role == null)
            {
                role = new Role { Name = roleName, Key = Guid.NewGuid() };
                _roleRepository.Add(role);
                _roleRepository.Save();
            }

            var userRole = new UserInRole { RoleKey = role.Key, UserKey = user.Key, Key = Guid.NewGuid() };
            _userInRoleRepository.Add(userRole);
            _userRepository.Save();
        }

        private Tuple<string, string> generateUserPassword_HashedPassword_Salt(string password)
        {
            var passwordSalt = _cryptoService.GenerateSalt();
            var hashedPassword = _cryptoService.EncryptPassword(password, passwordSalt);
            return Tuple.Create(hashedPassword, passwordSalt);
        }

        private UserWithRoles getUserWithRoles(User user)
        {
            IEnumerable<Role> userRoles = getUserRoles(user.Key);
            return new UserWithRoles { Roles = userRoles, User = user };
        }

        private IEnumerable<Role> getUserRoles(Guid userKey)
        {
            var userInRoles = _userInRoleRepository.FindBy(x => x.UserKey == userKey);
            if (userInRoles != null && userInRoles.Count() > 0)
            {
                var userRoleKeys = userInRoles.Select(x => x.RoleKey);
                var userRoles = _roleRepository.FindBy(x => userRoleKeys.Contains(x.Key));
                return userRoles;
            }
            return Enumerable.Empty<Role>();
        }

        public Role GetRole(string roleName)
        {
            return _roleRepository.FindBy(x => x.Name == roleName).FirstOrDefault();
        }

        public Role GetRole(Guid key)
        {
            return _roleRepository.GetSingle(key);
        }

        public IEnumerable<Role> GetRoles()
        {
            return _roleRepository.All;
        }

        public UserWithRoles GetUser(Guid userKey)
        {
            var user = _userRepository.GetSingle(userKey);
            return getUserWithRoles(user);
        }

        public UserWithRoles GetUser(string email)
        {
            var user = _userRepository.GetUserByEmail(email);
            return getUserWithRoles(user);
        }

        public PaginatedList<UserWithRoles> GetUsers(int pageIndex, int pageSize)
        {
            var users = _userRepository.All.OrderBy(x => x.CreatedOn).ToList();
            var usersWithRoles = users.Select(x => getUserWithRoles(x));
             return new PaginatedList<UserWithRoles>(pageIndex, pageSize, users.Count(), usersWithRoles.AsQueryable()); 
        }

        public bool RemoveFromRole(Guid key, string roleName)
        {
            return removeUserFromRole(key, null, roleName);
        }

        public bool RemoveFromRole(string email, string roleName)
        {
            return removeUserFromRole(null, email, roleName);
        }

        private bool removeUserFromRole(Guid? key, string email, string roleName)
        {
            var user = key == null ? _userRepository.GetUserByEmail(email) : _userRepository.GetSingle(key.Value);          
            var role = _roleRepository.GetRoleByRoleName(roleName);

            if (role != null && user != null)
            {
                var userInRole = _userInRoleRepository.FindBy(x => x.RoleKey == role.Key && x.UserKey == key).FirstOrDefault();
                if (userInRole != null)
                {
                    _userInRoleRepository.Delete(userInRole);
                    _userInRoleRepository.Save();

                    return true;
                }
            }

            return false;
        }

        public UserWithRoles UpdateUser(User user, string email, string companyName, string name, string phone)
        {
            user.Email = email;
            user.Name = name;
            user.CompanyName = companyName;
            user.Phone = phone;
            _userRepository.Edit(user);
            _userRepository.Save();
            return getUserWithRoles(user);
        }

        public ValidUserContext ValidateUserWithEmailPassword(string email, string password)
        {
            var userCtx = new ValidUserContext();
            var user = _userRepository.GetUserByEmail(email);
            if(user != null && isUserWithValidPassword(user, password))
            {
                setUserContext(userCtx, user);
            }
            return userCtx;
        }

        public ValidUserContext ValidateUserWithKeyReferer(string key, string referer)
        {
            var userCtx = new ValidUserContext();
            var user = _userRepository.GetUserByKey(key);
            if (user != null && isUserFromValidUrl(user, referer))
            {
                setUserContext(userCtx, user);
            }
            return userCtx;
        }

        private void setUserContext(ValidUserContext userCtx, User user)
        {
            var userRoles = getUserRoles(user.Key);
            userCtx.User = new UserWithRoles { User = user, Roles = userRoles };

            var identity = new GenericIdentity(user.Email);
            userCtx.Principal = new GenericPrincipal(identity, userRoles.Select(x => x.Name).ToArray());
        }

        private bool isUserWithValidPassword(User user, string password)
        {
            if (isPasswordValid(user, password))
                return !user.IsLocked;

            return false;
        }

        private bool isUserFromValidUrl(User user, string referer)
        {            
            if ((user.LiveUrl == null && referer == null) || user.LiveUrl.StartsWith(referer))
                return !user.IsLocked;

            return false;
        }

        private bool isPasswordValid(User user, string password)
        {
            return string.Equals(_cryptoService.EncryptPassword(password, user.HashedPassword), user.HashedPassword);
        }
    }
}
