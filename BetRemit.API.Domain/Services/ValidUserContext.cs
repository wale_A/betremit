﻿using BetRemit.Domain.ServiceModels;
using System.Security.Principal;

namespace BetRemit.Domain.Services
{
    public class ValidUserContext
    {
        public UserWithRoles User { get; set; }
        public IPrincipal Principal { get; set; }
    }
}