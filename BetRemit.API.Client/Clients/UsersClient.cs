﻿using BetRemit.API.Model.DTO;
using BetRemit.API.Model.RequestCommands;
using BetRemit.API.Model.RequestModel;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Client.Clients
{
    public class UsersClient : HttpApiClient<UserDTO>, IUsersClient
    {
        private const string BaseUriTemplate = "api/users";
        private const string BaseUriTemplateForSingle = "api/users/{key}";

        public UsersClient(HttpClient httpClient) : base(httpClient, MediaTypeFormatterCollection.Instance)
        { }

        public async Task<UserDTO> AddUserAsync(UserRequestModel requestModel)
        {
            var responseTask = base.PostAsync(BaseUriTemplate, requestModel);
            var user = await HandleResponseAsync(responseTask);
            return user;
        }

        public async Task<UserDTO> GetUserAsync(Guid userkey)
        {
            var parameter = new { key = userkey };
            var responseTask = base.GetSingleAsync(BaseUriTemplateForSingle, parameter);
            var user = await HandleResponseAsync(responseTask);
            return user;
        }

        public async Task<PaginatedDto<UserDTO>> GetUsersAsync(PaginatedRequestCommand cmd)
        {
            var parameters = new { page = cmd.Page, take = cmd.Take };
            var responseTask = base.GetAsync(BaseUriTemplate, parameters);
            var users = await HandleResponseAsync(responseTask);
            return users;
        }

        public async Task RemoveUserAsync(Guid userKey)
        {
            var parameters = new { key = userKey };
            var responseTask = base.DeleteAsync(BaseUriTemplate, parameters);
            await HandleResponseAsync(responseTask);
        }

        public async Task<UserDTO> UpdateUserAsync(Guid userKey, UserRequestModel requestModel)
        {
            var parameters = new { key = userKey };
            var responseTask = base.PutAsync(BaseUriTemplate, requestModel, parameters);
            var user = await HandleResponseAsync(responseTask);
            return user;
        }

        private async Task<TResult> HandleResponseAsync<TResult>(Task<HttpApiResponseMessage<TResult>> responseTask)
        {
            using (var apiResponse = await responseTask)
            {
                if (apiResponse.IsSuccess)
                    return apiResponse.Model;

                throw getHttpApiRequestException(apiResponse);
            }
        }

        private Exception getHttpApiRequestException(HttpApiResponseMessage apiResponse)
        {
            return new HttpApiRequestException(string.Format(ErrorMessages.HttpRequestErrorFormat, (int)apiResponse.Response.StatusCode, apiResponse.Response.ReasonPhrase), apiResponse.Response.StatusCode, apiResponse.HttpError);
        }

        private async Task HandleResponseAsync(Task<HttpApiResponseMessage> responseTask)
        {
            using (var apiResponse = await responseTask)
                if (!apiResponse.IsSuccess)
                    throw getHttpApiRequestException(apiResponse);
        }
    }
}
