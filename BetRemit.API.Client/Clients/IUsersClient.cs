﻿using BetRemit.API.Model.DTO;
using BetRemit.API.Model.RequestCommands;
using BetRemit.API.Model.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Client.Clients
{
    public interface IUsersClient
    {
        Task<PaginatedDto<UserDTO>> GetUsersAsync(PaginatedRequestCommand cmd);
        Task<UserDTO> GetUserAsync(Guid userkey);
        Task<UserDTO> AddUserAsync(UserRequestModel requestModel);
        Task<UserDTO> UpdateUserAsync(Guid userKey, UserRequestModel requestModel);
        Task RemoveUserAsync(Guid userKey);
    }
}
