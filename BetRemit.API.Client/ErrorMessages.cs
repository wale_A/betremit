﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Client
{
    internal class ErrorMessages
    {
        internal const string HttpRequestErrorFormat = "Response status code does not indicate sucess: {0} ({1})";
    }
}
