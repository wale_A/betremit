﻿using System;

namespace BetRemit.API.Model.DTO
{
    public class RoleDTO
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
    }
}
