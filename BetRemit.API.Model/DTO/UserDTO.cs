﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Model.DTO
{
    public class UserDTO : IDto
    {
        public Guid Key { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public bool IsLocked { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Phone { get; set; }
        public bool IsLive { get; set; }
        public string TestPublicKey { get; set; }
        public string TestSecretKey { get; set; }
        public string LiveUrl { get; set; }

        public IEnumerable<PunterDTO> Punters { get; set; }
    }
}
