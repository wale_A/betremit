﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Model.DTO
{
    public class VirtualCardDTO : IDto
    {
        public Guid Key{ get; set; }
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public string CVV { get; set; }

        public Guid PunterKey { get; set; }
    }
}
