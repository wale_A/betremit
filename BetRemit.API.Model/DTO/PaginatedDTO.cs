﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Model.DTO
{
    public class PaginatedDTO<T> : IPaginatedDTO<T>
        where T : IDto
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPageCount { get; set; }
        public bool HasNextPage { get; set; }
        public bool HasPrevious { get; set; }

        public IEnumerable<T> items { get; set; }
    }
}
