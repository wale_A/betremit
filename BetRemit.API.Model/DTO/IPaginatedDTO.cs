﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Model.DTO
{
    public interface IPaginatedDTO<out T>
        where T: IDto
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
        int TotalCount { get; set; }
        int TotalPageCount { get; set; }

        bool HasNextPage { get; set; }
        bool HasPrevious { get; set; }

        IEnumerable<T> items { get; }
    }
}
