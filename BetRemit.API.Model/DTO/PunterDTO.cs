﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiDoodle.Net.Http.Client.Model;

namespace BetRemit.API.Model.DTO
{
    public class PunterDTO : IDto
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string Phone { get; set; }
        public string Reference { get; set; }
        public DateTime CreatedOn { get; set; }

        public Guid CreatedByKey { get; set; }
        public UserDTO CreatedBy { get; set; }
    }
}
