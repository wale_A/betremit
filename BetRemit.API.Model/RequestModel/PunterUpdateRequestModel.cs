﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Model.RequestModel
{
    public class PunterUpdateRequestModel
    {
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string Phone { get; set; }
    }
}
