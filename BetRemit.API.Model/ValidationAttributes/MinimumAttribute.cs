﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Model.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MinimumAttribute: ValidationAttribute
    {
        private readonly int _min;

        public MinimumAttribute(int minimumValue)
            : base()
        {
            _min = minimumValue;
            this.ErrorMessage = "The {0} field must have a minimum of {1}";
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, this.ErrorMessage, name, _min);
        }

        public override bool IsValid(object value)
        {
            int intValue;
            if (value != null && int.TryParse(value.ToString(), out intValue))
                return _min <= intValue;

            return false;
        }
    }
}
