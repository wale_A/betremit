﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRemit.API.Model.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MaximumAttribute : ValidationAttribute
    {
        private readonly int _max;

        public MaximumAttribute(int max)
            : base(errorMessage: "The {0} field musty be maximum {1}.")
        {
            _max = max;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, base.ErrorMessage, name, _max);
        }

        public override bool IsValid(object value)
        {
            int intValue;
            if (value != null && int.TryParse(value.ToString(), out intValue))
                return _max >= intValue;

            return false;
        }

    }
}
